<?php
App::uses('AppController', 'Controller');
/**
 * Comments Controller
 *
 * @property Comment $Comment
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class CommentsController extends AppController {

	// public function add() {
	// 	if ($this->request->is('post')) {
	// 		$this->Comment->create();
		
	// 		if ($this->Comment->save($this->request->data)) {
	// 			$this->Session->setFlash(__('The comment has been saved.'));
	// 			return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
	// 		} else {
	// 			$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
	// 		}
	// 	}
	// }

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

}
