<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

	public $uses = array('User', 'Post');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('add');
	}
	
	public function login(){
		if($this->request->is('post')){
			if($this->Auth->login()) {
				$this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash('Your username and password combination was incorrect');
			}
		}
	}

	public function isAuthorized($user)
	{
		if ($user['role'] == 'admin'){
			return true;
		}
		if(in_array($this->action, array('edit','delete'))) {
			if($user['id'] != $this->request->params['pass'][0]) {
				return false;
			}
		}
		return true;
	}

	public function logout(){
		$this->redirect($this->Auth->logout());
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		// debug($this->User->findById(
		// 	$this->Auth->user('id')
		// )
		// );
		// debug();
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
        if ($this->request->is('post')) {
            // debug($this->request->data);
            if ($this->User->Post->save($this->request->data)) {
                $this->Session->setFlash(_('Add Post Successfuly naja jub'));
                return $this->redirect(array('action' => 'view',$id));
            } else {
                $this->Session->setFlash(_('Unable to add your posts.'));
                // debug($errors = $this->User->Post->validationErrors);
			}
		}
		

            
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $user = $this->User->find('first', $options);
            $post = $this->User->Post->find(
            'all',
            array(
                'conditions' => array('Post.user_id' => $id))
        );
            $this->set(compact('user', 'post'));
        
	}

	public function commentAjax($id = null){
		$this->autoRender = false;
		$this->request->data['Comment']['user_id'] = $this->Auth->user('id');
		$this->request->data['Comment']['username'] = $this->Auth->user('username');

		$comment = $this->request->data;
		if (isset($comment['Comment']['body'])) {
			$comment = $this->Post->Comment->save($comment['Comment']);

				
			if (isset($comment)) {
				// $this->Session->setFlash(_('Add Comment Successfully'));
				return json_encode($comment);
				// return $this->redirect(array('action' => 'view', $id));
			} else {
				// $this->Session->setFlash(_('Unable to add your comment.'));
			}
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
