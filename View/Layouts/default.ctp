<?php
/**
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body>
	<div id="container">
		<!-- <div id="header">
			
		</div> -->
		<div id="content">

			<div style="text-align: right;">
				<?php if ($logged_in):?>
				<nav class="navbar navbar-expand-sm bg-light">
				<ul class="navbar-nav"  style="margin-left:85%">	
						<li class="nav-item">
							Welcome <?php echo $current_user['username'];?>. <?php echo $this->Html->link('Logout', array('controller'=>'users', 'action'=>'logout')); ?>
						</li>				
						</ul>	
				</nav>
					<?php else:?>
				<nav class="navbar navbar-expand-sm bg-light">
					<ul class="navbar-nav"  style="margin-left:95%">	
						<li class="nav-item">
							<?php echo $this->Html->link('Login', array('controller'=>'users', 'action'=>'login')); ?>
						</li>				
					</ul>	
				</nav>
				<?php endif;?>
			</div>

			<?php echo $this->Session->flash(); ?>
			<?php echo $this->Session->flash('auth'); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<!-- <div id="footer">
		
		</div> -->
	</div>

	<script>
	// $(document).ready(function(){
		$('.insert').on('click',function(e){
			console.log($(this).parent().attr("class"));
			var summit = '.' + $(this).parent().attr("class")

			console.log($(summit).serialize());
			$.ajax({
				url:"/Artza/users/commentajax",
				method:"POST",
				data:$(summit).serialize(),
				beforeSend:function(){
					$(this).val("Insert.....");
				},
				success:function(data){
					console.log(data)
					var data = jQuery.parseJSON(data);
					//  $('.card-footer').append(data.Comment.body + "<br>" + "<br>" 
					//  + "By: " + data.Comment.username + "&nbsp" + "&nbsp" + "&nbsp" + "&nbsp" + data.Comment.created + "<hr>");
					location.reload();
					// alert("successfully")
				}
			});
		});
	// });
</script>

	
</body>
</html>
