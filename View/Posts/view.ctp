<div class="posts view">
<h2><?php echo __('Post'); ?></h2>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header">		
			<br>
				<dd>
					<h3><?php echo h($post['Post']['title']); ?></h3>
					&nbsp;			
				</dd>		
				<dd>
					<?php echo h($post['Post']['created']); ?>
					&nbsp;
				</dd>
			</div>
		
			<div class="card-body">
			<br>
				<dd>
					<h5><?php echo h($post['Post']['body']); ?></h5>
					&nbsp;
				</dd>
			</div>		
			<br>
		
		<!-- <dt><?php echo __('Modified'); ?></dt>
		<br>
		<dd>
			<?php echo h($post['Post']['modified']); ?>
			&nbsp;
		</dd> -->

			<div class="card-footer">
				<?php foreach($post['Comment'] as $comment):?>
				<p><?php echo h($comment['body']);?></p>
				<p>
					<?php echo __('By: ');?>
					<?php echo h($comment['username']);?>
					&nbsp; &nbsp;
					<?php echo $comment['created'];?></p>
				<hr>
				<?php endforeach;?>
				
				<?php
					echo $this->Form->create('Comment');
					echo $this->Form->hidden('post_id',array('value' => $post['Post']['id']));?>
					<div class="input textarea">
						<label for="CommentBody"></label>
						<input name="data[Comment][body]" cols="30" rows="6" id="CommentBody" >
					</div>
					<?php echo $this->Form->end('Add Comment');?>
			</div>
		</div>

	
</div>
</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?> </li>
	<!-- <?php if ($current_user['id'] == $user['User']['id'] || $current_user['role'] == 'admin'): ?> -->
		<li><?php echo $this->Html->link(__('Edit Post'), array('action' => 'edit', $post['Post']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Post'), array('action' => 'delete', $post['Post']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $post['Post']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('action' => 'add')); ?> </li>
	<?php endif;?>
	</ul>
</div>
