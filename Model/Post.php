<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 */
class Post extends AppModel {
    public $hasMany = array(
        'Comment' => array(
            'className' => 'Comment',
            'foreignKey' => 'post_id',
            'dependent' => true
        )
    );

    public $validate = array(
        'title' => array(
            'rule'=>'notBlank',
            'requird' => true,
            'message' => 'ใส่ไม่ได้เว้ยยยยยยย'
        ),
        'body' => array(
            'rule' => 'notBlank',
            'required' => true,
            'message' => 'ไปใส่มาใหม่นะจ้ะ'
        )
        );
}
